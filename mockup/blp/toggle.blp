using Gtk 4.0;
using Adw 1;

Adw.ApplicationWindow {
  width-request: 360;
  height-request: 294;
  default-width: 800;
  default-height: 650;
  content: Adw.ViewStack {

    Adw.ViewStackPage {
      child: Adw.ToolbarView {

        [top]
        Adw.HeaderBar header_bar {
          title-widget: Adw.ViewSwitcher {
            stack: stack;
          };

          [end]
          MenuButton primary {
            halign: center;
            icon-name: "open-menu-symbolic";
            menu-model: primary_button_menu;
            primary: true;
          }
        }

        content: Adw.ViewStack stack {

          Adw.ViewStackPage {
            title: _("Appearance");
            icon-name: "larger-brush-symbolic";
            child: Adw.PreferencesPage {

              Adw.PreferencesGroup {
                title: _("Themes");
                header-suffix: Button {
                  icon-name: "info";

                  styles [
                    "flat"
                  ]
                };

                Adw.ComboRow {
                  title: _("Cursor");
                  model: StringList {
                    strings [
                      "Adwaita (Default)"
                    ]
                  };
                }

                Adw.ComboRow {
                  title: _("Icons");
                  model: StringList {
                    strings [
                      "Adwaita (Default)",
                      "Hicolor",
                      "HighContrast"
                    ]
                  };
                }
              }

              Adw.PreferencesGroup {
                title: _("Fonts");

                Adw.ActionRow {
                  title: _("Applications");

                  [suffix]
                  FontDialogButton {
                    valign: center;
                    use-font: true;
                    use-size: true;
                  }
                }
          
                Adw.ActionRow {
                  title: _("Documents");

                  [suffix]
                  FontDialogButton {
                    valign: center;
                    use-font: true;
                    use-size: true;
                  }
                }

                Adw.ActionRow {
                  title: _("Monospace");

                  [suffix]
                  FontDialogButton {
                    valign: center;
                    use-font: true;
                    use-size: true;
                  }
                }

                Adw.ComboRow {
                  title: _("Hinting");

                  model: StringList {
                    strings [
                      _("Full"),
                      _("Medium"),
                      _("Slight"),
                      _("None")
                    ]
                  };
                }
          
                Adw.ComboRow {
                  title: _("Anti-aliasing");

                  model: StringList {
                    strings [
                      _("Subpixel"),
                      _("Standard"),
                      _("None")
                    ]
                  };
                }

                Adw.SpinRow {
                  title: _("Scaling Factor");
                  digits: 2;

                  adjustment: Adjustment {
                    lower: 0.5;
                    upper: 3;
                    step-increment: 0.25;
                    value: 1;
                  };
                }
              }
            };
          }
          
          Adw.ViewStackPage {
            title: _("Desktop");
            icon-name: "display-with-window-symbolic";
            child: Adw.PreferencesPage {

              Adw.PreferencesGroup {
                title: _("Windows");
          
                Adw.SwitchRow {
                  title: _("Center New Windows");
                }

                Adw.ActionRow {
                  title: _("Resize Shortcut");

                  [suffix]
                  Box {
                    valign: center;

                    styles [
                      "linked"
                    ]

                    ToggleButton middle_click {
                      label: _("Middle Click");
                      active: true;
                    }

                    ToggleButton secondary_click {
                      label: _("Secondary Click");
                      group: middle_click;
                    }
                  }
                }
          
                Adw.ComboRow {
                  title: _("Window Action Key");

                  model: StringList {
                    strings [
                      "Alt",
                      _("None"),
                      "Super",
                    ]
                  };
                }
              }
        
              Adw.PreferencesGroup {
                title: _("Header Bar");

                Adw.ExpanderRow {
                  title: _("Actions");

                  Adw.ComboRow {
                    title: _("Double Click");

                    model: StringList {
                      strings [
                        _("Menu"),
                        _("Minimize"),
                        _("None"),
                        _("Toggle Maximize"),
                        _("Toggle Maximize Horizontally"),
                        _("Toggle Maximize Vertically")
                      ]
                    };
                  }

                  Adw.ComboRow {
                    title: _("Middle Click");

                    model: StringList {
                      strings [
                        _("Menu"),
                        _("Minimize"),
                        _("None"),
                        _("Toggle Maximize"),
                        _("Toggle Maximize Horizontally"),
                        _("Toggle Maximize Vertically")
                      ]
                    };
                  }

                  Adw.ComboRow {
                    title: _("Secondary Click");

                    model: StringList {
                      strings [
                        _("Menu"),
                        _("Minimize"),
                        _("None"),
                        _("Toggle Maximize"),
                        _("Toggle Maximize Horizontally"),
                        _("Toggle Maximize Vertically")
                      ]
                    };
                  }
                }

                Adw.ExpanderRow {
                  title: _("Buttons");

                  Adw.ActionRow {
                    title: _("Alignment");

                    [suffix]
                    Box {
                      valign: center;

                      styles [
                        "linked"
                      ]

                      // Using Start/End instead of Left/Right
                      // for RTL language support
                      ToggleButton button_left {
                        label: "Start";
                      }

                      ToggleButton button_right {
                        active: true;
                        label: "End";
                        group: button_left;
                      }
                    }
                  }

                  Adw.SwitchRow {
                    title: _("Minimize");
                  }

                  Adw.SwitchRow {
                    title: _("Maximize");
                  }
                }
              }

              Adw.PreferencesGroup {
                title: _("Shell");

                Adw.SwitchRow {
                  title: _("Fractional Scaling");
                }

                Adw.SwitchRow {
                  title: _("Light Style");
                }
              }
            };
          }

          Adw.ViewStackPage {
            title: _("Devices");
            icon-name: "input-mouse-symbolic";
            child: Adw.PreferencesPage {

              Adw.PreferencesGroup {
                title: _("Laptop");

                Adw.SwitchRow {
                  title: _("Suspend When Laptop Is Closed");
                  active: true;
                }

                Adw.SwitchRow {
                  title: _("Disable Touchpad While Typing");
                  active: true;
                }
              }

              Adw.PreferencesGroup {
                title: _("Peripherals");

                Adw.ActionRow {
                  title: _("Overview Shortcut");

                  [suffix]
                  Box {
                    valign: center;

                    styles [
                      "linked"
                    ]

                    ToggleButton left_super {
                      active: true;
                      label: _("Left Super");
                    }

                    ToggleButton right_super {
                      label: _("Right Super");
                      group: left_super;
                    }
                  }
                }
          
                Adw.SwitchRow {
                  title: _("Middle Click Paste");
                  active: true;
                }
              }

              Adw.PreferencesGroup {
                title: _("Mouse Click Emulation");

                Adw.ActionRow {
                  title: _("Fingers");
                  subtitle: _("Click the touchpad with two fingers for right-click and three fingers for middle-click");

                  [prefix]
                  CheckButton fingers {
                    active: true;
                  }
                }

                Adw.ActionRow {
                  title: _("Area");
                  subtitle: _("Click the bottom right of the touchpad for right-click and the bottom middle for middle-click");

                  [prefix]
                  CheckButton area {
                    group: fingers;
                  }
                }

                Adw.ActionRow {
                  title: _("Disabled");

                  [prefix]
                  CheckButton disabled {
                    group: area;
                  }
                }
              }
            };
          }
          
          Adw.ViewStackPage {
            title: _("Startup Apps");
            icon-name: "media-playback-start-symbolic";
            child: Adw.PreferencesPage {

              Adw.PreferencesGroup {
                title: "Apps";
                header-suffix: Button {
                  icon-name: "plus-large-symbolic";

                  styles [
                    "flat"
                  ]
                };

                Adw.ActionRow {
                  title: "Amberol";

                  [prefix]
                  Image {
                    icon-name: "io.bassi.Amberol";
                    icon-size: large;
                  }

                  [suffix]
                  Button {
                    valign: center;
                    icon-name: "user-trash-symbolic";

                    styles [
                      "flat"
                    ]
                  }
                }

                Adw.ActionRow {
                  title: "Web";

                  [prefix]
                  Image {
                    icon-name: "org.gnome.Epiphany";
                    icon-size: large;
                  }

                  [suffix]
                  Button {
                    valign: center;
                    icon-name: "user-trash-symbolic";

                    styles [
                      "flat"
                    ]
                  }
                }

                Adw.ActionRow {
                  title: "NewsFlash";

                  [prefix]
                  Image {
                    icon-name: "io.gitlab.news_flash.NewsFlash";
                    icon-size: large;
                  }

                  [suffix]
                  Button {
                    valign: center;
                    icon-name: "user-trash-symbolic";

                    styles [
                      "flat"
                    ]
                  }
                }
              }
            };
          }
        };
      };
    }

    Adw.ViewStackPage {
      child: Adw.ToolbarView {

        [top]
        Adw.HeaderBar {
          title-widget: Adw.WindowTitle {
            title: "Toggle";
          };
        }

        content: Adw.StatusPage {
          valign: center;
          hexpand: true;
          vexpand: true;
          
          title: _("Unsupported Options");
          description: _("Toggle provides access to options which may be unsupported by GNOME. Use at own discretion.");
          icon-name: "warning";

          Button {
            label: _("Continue");
            halign: center;
            styles [
              "pill",
              "suggested-action"
            ]
          }
        };
      };
    }
  };
}

menu primary_button_menu {
    item {
      label: _("Keyboard Shortcuts");
      action: "app.shortcuts";
    }

    item {
      label: _("About Workbench");
      action: "app.about";
    }
}
