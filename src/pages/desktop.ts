import Adw from "gi://Adw";
import Gio from "gi://Gio";
import GObject from "gi://GObject";
import Gtk from "gi://Gtk?version=4.0";

import { bind_setting, bind_setting_flag, get_settings, bind_setting_enum } from "./util.js";

export class ToggleDesktopPreferencePage extends Adw.PreferencesPage {
  private _center_new_windows!: Adw.SwitchRow;
  private _resize_middle_click!: Gtk.ToggleButton;
  private _resize_secondary_click!: Gtk.ToggleButton;
  private _action_key!: Adw.ComboRow;
  private _header_double_click!: Adw.ComboRow;
  private _header_middle_click!: Adw.ComboRow;
  private _header_secondary_click!: Adw.ComboRow;
  private _header_align_start!: Gtk.ToggleButton;
  private _header_align_end!: Gtk.ToggleButton;
  private _header_buttons_minimise!: Adw.SwitchRow;
  private _header_buttons_maximise!: Adw.SwitchRow;
  private _fractional_scaling!: Adw.SwitchRow;
  private _light_style!: Adw.SwitchRow;
  private _header_align_row!: Adw.ActionRow;

  private _wm_prefs: Gio.Settings | null = null;

  static {
    GObject.registerClass(
      {
        GTypeName: "ToggleDesktopPreferencePage",
        Template: "resource:///io/gitlab/orowith2os/Toggle/ui/pages/desktop.ui",
        InternalChildren: [
          "center_new_windows",
          "resize_middle_click",
          "resize_secondary_click",
          "action_key",
          "header_double_click",
          "header_middle_click",
          "header_secondary_click",
          "header_align_start",
          "header_align_end",
          "header_buttons_minimise",
          "header_buttons_maximise",
          "fractional_scaling",
          "light_style",
          "header_align_row",
        ],
        Signals: {
          warning_accepted: {},
        },
      },
      this,
    );
  }

  constructor(params?: Partial<Adw.PreferencesPage.ConstructorProperties>) {
    super(params);

    bind_setting(
      "org.gnome.mutter",
      "center-new-windows",
      this._center_new_windows,
    );

    bind_setting(
      "org.gnome.desktop.wm.preferences",
      "resize-with-right-button",
      this._resize_middle_click,
      Gio.SettingsBindFlags.DEFAULT | Gio.SettingsBindFlags.INVERT_BOOLEAN,
    );

    bind_setting(
      "org.gnome.desktop.wm.preferences",
      "resize-with-right-button",
      this._resize_secondary_click,
    );

    bind_setting_flag(
      "org.gnome.mutter",
      "experimental-features",
      this._fractional_scaling,
      "active",
      "scale-monitor-framebuffer",
    );

    bind_setting_enum(
      "org.gnome.desktop.interface",
      "color-scheme",
      this._light_style,
      "active",
      "prefer-light",
      "default"
    );

    this.setup_wm_prefs();
  }

  private setup_wm_prefs() {
    this._wm_prefs = get_settings("org.gnome.desktop.wm.preferences");

    if (!this._wm_prefs) {
      this._header_align_row.sensitive =
        this._header_buttons_minimise
          .sensitive =
        this._header_buttons_maximise.sensitive =
          false;
      return;
    }

    this._wm_prefs.connect(
      "changed::button-layout",
      this.refresh_wm_prefs_buttons.bind(this),
    );

    this.refresh_wm_prefs_buttons();
  }

  private refresh_wm_prefs_buttons() {
    if (!this._wm_prefs) return;

    const layout = this._wm_prefs.get_string("button-layout");

    this._header_align_start.active = layout.startsWith("close");
    this._header_align_end.active = layout.endsWith("close");
    this._header_buttons_minimise.active = layout.includes("minimize");
    this._header_buttons_maximise.active = layout.includes("maximize");
  }

  private header_buttons_changed_cb() {
    if (!this._wm_prefs) return;

    const buttons = [];

    if (this._header_buttons_minimise.active) {
      buttons.push("minimize");
    }

    if (this._header_buttons_maximise.active) {
      buttons.push("maximize");
    }

    const buttons_string = buttons.join(",");

    const string = this._header_align_start.active
      ? `close${buttons_string.length > 0 ? "," : ""}${buttons_string}:appmenu`
      : `appmenu:${buttons_string}${buttons_string.length > 0 ? "," : ""}close`;

    this._wm_prefs.set_string(
      "button-layout",
      string,
    );
  }
}
