import Adw from "gi://Adw";
import GObject from "gi://GObject";

import { ToggleAppearancePreferencePage } from "./pages/appearance.js";
import { ToggleDesktopPreferencePage } from "./pages/desktop.js";
import { ToggleDevicesPreferencePage } from "./pages/devices.js";
import { ToggleStartupAppsPreferencePage } from "./pages/startup-apps.js";

ToggleAppearancePreferencePage;
ToggleDesktopPreferencePage;
ToggleDevicesPreferencePage;
ToggleStartupAppsPreferencePage;

export class TogglePreferencesWindow extends Adw.Bin {
  static {
    GObject.registerClass(
      {
        GTypeName: "TogglePreferencesWindow",
        Template: "resource:///io/gitlab/orowith2os/Toggle/ui/preferences.ui",
      },
      this,
    );
  }

  constructor(params?: Partial<Adw.Bin.ConstructorProperties>) {
    super(params);
  }
}
