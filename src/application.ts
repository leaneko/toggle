import Adw from "gi://Adw";
import Gio from "gi://Gio";
import GLib from "gi://GLib";
import GObject from "gi://GObject";
import Gtk from "gi://Gtk?version=4.0";

import { ToggleWindow } from "./window.js";

export const Settings = new Gio.Settings({ schema: pkg.name });

export class ToggleApplication extends Adw.Application {
  private window?: ToggleWindow;

  static {
    GObject.registerClass(this);
  }

  constructor() {
    super({
      application_id: pkg.name,
      flags: Gio.ApplicationFlags.DEFAULT_FLAGS,
      resource_base_path: "/io/gitlab/orowith2os/Toggle/"
    });

    const quit_action = new Gio.SimpleAction({ name: "quit" });
    quit_action.connect("activate", () => {
      this.quit();
    });

    this.add_action(quit_action);
    this.set_accels_for_action("app.quit", ["<Control>q"]);

    const show_about_action = new Gio.SimpleAction({ name: "about" });
    show_about_action.connect("activate", () => {
      new Adw.AboutWindow({
        transient_for: this.active_window,
        modal: true,
        application_name: _("Toggle"),
        application_icon: pkg.name,
        developer_name: "Dallas Strouse",
        version: pkg.version,
        license_type: Gtk.License.GPL_3_0,

        debug_info: get_debug_info(),
        debug_info_filename: "toggle-debuginfo.txt",
        issue_url: "https://gitlab.com/orowith2os/toggle/-/issues/new",
        support_url: "https://matrix.to/#/#toggle-application:fedora.im",

        // Translators: Replace "translator-credits" with your names, one name per line
        translator_credits: _("translator-credits"),
        developers: ["Dallas Strouse <dastrouses@gmail.com>", "Angelo Verlain <geoangercola@gmail.com>", "Maxim Therrien <maxim@veryloud.ca>"],
        designers: [
          "Bart Gravendeel",
          "Brage Fuglseth https://bragefuglseth.dev",
        ],
        artists: ["Brage Fuglseth https://bragefuglseth.dev"],
        copyright: "© 2023 Dallas Strouse",
      }).present();
    });

    this.add_action(show_about_action);

    Gio._promisify(Gtk.UriLauncher.prototype, "launch", "launch_finish");
  }

  public vfunc_activate(): void {
    if (!this.window) {
      this.window = new ToggleWindow({ application: this });
    }

    this.window.present();
  }
}

function get_debug_info(): string {
  return (
    `Toggle version: ${pkg.version}\n` +
    `AppID: ${GLib.getenv("FLATPAK_ID") ?? "Not set"}\n` +
    `Prefix: ${pkg.prefix}\n` +
    `Libdir: ${pkg.libdir}\n` +
    `Datadir: ${pkg.datadir}\n` +
    `Display server: ${GLib.getenv("XDG_SESSION_TYPE") ?? "Not set"}\n` +
    `Desktop environment: ${GLib.getenv("XDG_CURRENT_DESKTOP") ?? "Not set"}\n`
  );
}
